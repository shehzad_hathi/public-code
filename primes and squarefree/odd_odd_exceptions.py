import multiprocessing as mproc
import pickle
from time import time
from bisect import bisect
from math import gcd
from primesieve import primes
from sympy import primefactors, factorint, mobius

#########################################
# Returns all odd squarefree n (m<=n<=M)
# with no. of distinct prime factors <= 2
#########################################
def get_semiprimes(m,M):
    # we assume that m is odd
    return filter(lambda x: mobius(x) and len(primefactors(x)) <= 2,range(m,M+1,2))

n_max = 8*(10**9)
k_max = 100000
# List of all primes <= 600
relevant_primes = primes(600)
# List of all odd semiprimes (including primes) <= 100000
k_semiprimes = set(get_semiprimes(1,k_max))
# A boolean list whose i-th index is True if (i+1) is a squarefree number (False otherwise)
with open("sqfree_indicator_160M.pkl","rb") as pkl_file:
    sqfree_indicator = pickle.load(pkl_file)

############################################
# Checks if n can be represented as the sum
# of a prime and squarefree no. coprime to k
############################################
def is_exception(n,k):
    for p in relevant_primes[:bisect(relevant_primes,n-2)]:
        m = n-p
        if mobius(m) and gcd(m,k) == 1:
            return False
    return True

############################################
# Checks if three no. have pairwise gcd <= 2
############################################
def permissible_trio(s):
    return (gcd(s[0],s[1]) <= 2 and gcd(s[1],s[2]) <= 2 and gcd(s[0],s[2]) <= 2)

############################################
# Implements the algorithm described in the
# proof of T5.5 for n >= 601
############################################
def is_n_exception_FLtrick(n,prime_list):
    existing_sqfree_twins = []
    contending_sqfree = []
    for p in prime_list:
        s = n-p
        if sqfree_indicator[s-1]:
            if existing_sqfree_twins:
                if any(map(permissible_trio,map(lambda x: x+[s],existing_sqfree_twins))):
                    return (n,False)
            existing_sqfree_twins.extend([[s,x] for x in contending_sqfree])
            contending_sqfree.append(s)
    return (n,True)

####################################
# Brute force algorithm for n <= 600
####################################
def is_n_exception(n):
    suspect_k_semiprimes = k_semiprimes
    for p in relevant_primes[:bisect(relevant_primes,n-2)]:
        factor_dict = factorint(n-p)
        if all(map(lambda x: x<2, factor_dict.values())):
            suspect_k_semiprimes = {k for k in suspect_k_semiprimes if any(map(lambda p:k%p==0,factor_dict.keys()))}
            if not(suspect_k_semiprimes):
                return 0
    return (n,suspect_k_semiprimes)

###############################################
# Break up the range [m,M] into p roughly
# equal parts for regular updates while running
###############################################
def chunkify(m,M,p):
    l = (M-m+1)//p
    chunks = [(m+i*l,m-1+(i+1)*l) for i in range(p-1)]
    chunks.append((m+(p-1)*l,M))
    return chunks

###############################################
# Apply the function is_n_exception_FLtrick
# on odd numbers in the range [m,M) with
# parallel processing across given no. of cores
###############################################
def parallel_design(cores,m,M):
    prime_list = primes(m-1)[-100:]
    pool = mproc.Pool(cores)
    # we assume that m is odd
    results = pool.starmap_async(is_n_exception_FLtrick,[(i,prime_list) for i in range(m,M,2)]).get()
    pool.close()
    pool.join()
    not_exceptions, poss_exceptions = [], []
    for (n,v) in results:
        (not_exceptions,poss_exceptions)[v].append(n)
    return (not_exceptions,poss_exceptions)

def main():
    cores = 4 # 192 on NCI
    chunks = chunkify(601,n_max,1000)
    for (m,M) in chunks:
        if m%2 == 0:
            m = m+1
        st = time()
        bins = parallel_design(cores,m,M+1)
        et = time()
        with open("updates.txt","a") as log_file:
            log_file.write(str(len(bins[0]))+", "+str(len(bins[1]))+", "+str(et-st)+"\n")
    for n in range(5,600,2):
        res = is_n_exception(n)
        if res:
            print(res)

if __name__ == "__main__":
    main()