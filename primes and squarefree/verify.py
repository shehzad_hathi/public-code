import numpy as np
import pickle

from time import time
from itertools import combinations
from functools import reduce
from bisect import bisect
from math import gcd
from sympy import divisors, mobius, isprime, primefactors
from primesieve import primes
from mpmath import *

# Set decimal precision
mp.dps = 100

# Pre-calculated prime factorisation of relevant integers
with open("prime_factorisation.pkl","rb") as prime_factor_pkl:
    prime_factor_dict = pickle.load(prime_factor_pkl)

# Table of c_theta values from Broadbent et al
with open("ctheta_values.pkl","rb") as ctheta_pkl:
    ctheta_df = pickle.load(ctheta_pkl)

# List of all primes <= 400000
relevant_primes = np.array(primes(400000))

# Pre-calculated values of a using find_a_values function
with open("a_values.pkl","rb") as a_values_pkl:
    a_values_dict = pickle.load(a_values_pkl)

##################################
# Returns all divisors of m
# and the divisors of each divisor
##################################
def create_divisor_sets(m):
    div = np.array(divisors(m))
    div_sets = []
    for i in range(len(div)-1):
        d = div[i]
        dd = div[:i+1]
        div_sets.append((d,dd[d%dd==0]))
    div_sets.append((m,div))
    return div,div_sets

##################################
# Returns \phi(a^2) from the
# prime factorisation of a
##################################
def euler_totient_sq(prime_factors):
    return np.prod(prime_factors*(prime_factors-1))

##################################
# Returns \phi(d/e) from the
# prime factorisation of d and e
##################################
def euler_totient_de(prime_factors_d,prime_factors_e):
    try:
        return reduce(fmul,set(prime_factors_d)-set(prime_factors_e))
    except TypeError:
        return 1

#######################################
# Returns c_theta(n) for n > 1
# and appropriate c_theta(1) for
# L5.1,L5.4,T5.6 (res_code=0,1,2 resp.)
#######################################
def ctheta(n,res_code):
    n = int(n)
    try:
        return ctheta_df['c_theta'][n]
    except KeyError:
        if n == 1:
            return [fmul(8.6315,power(10,-7)),fmul(9.5913,power(10,-4)),fmul(6.3417,power(10,-9))][res_code]
        else:
            print("n = "+str(n)+" not permissible!")
            return

##################################
# Returns all squarefree a
# such that a \le \sqrt(10^5*e/d)
# and gcd(a,d) = e
##################################
def find_a_values(d,e):
    try:
        return a_values_dict[(d,e)]
    except KeyError:
        # print(d,e)
        max_range = int(sqrt(fmul(100000,e)/d)/e)
        a_values = [e*i for i in range(1,max_range+1) if gcd(i,d) == 1 and mobius(i)]
        a_values_dict[(d,e)] = a_values
        return a_values

#######################################
# Returns the (main) product term on
# the RHS of (5.2) & (5.3) mult. by c
#######################################
def main_product(k_divisors,c):
    p = np.prod(list(map(lambda q: 1-(q-1)/(fmul(q,q)-q-1),filter(isprime,k_divisors))))
    return p*c

########################################
# Returns the (first) triple sum on the
# RHS of (5.2) & (5.3) mult. by 1/log(n)
########################################
def ctheta_triple_sum(k_div_sets,n,res_code):
    s = 0
    for d,e_values in k_div_sets:
        for e in e_values:
            a_values = find_a_values(d,e)
            s += sum([ctheta(d*a*a/e,res_code) for a in a_values])
    return s/log(n)

######################################
# Returns the (second) triple sum on
# the RHS of (5.2) & (5.3) multiplied
# by (1+2C)/(1-2C)
######################################
def euler_totient_triple_sum(k_div_sets,C):
    s = 0
    for d,e_values in k_div_sets:
        totient_div_sum = np.prod([1+fmul(p,(p-1)) for p in prime_factor_dict[d]])
        totient_d_sq = euler_totient_sq(prime_factor_dict[d])
        for e in e_values:
            a_values = find_a_values(d,e)
            inner_sum_t1 = (zeta(2)*zeta(3)*totient_d_sq)/(zeta(6)*euler_totient_sq(prime_factor_dict[e])*totient_div_sum)
            inner_sum_t2 = sum([1/euler_totient_sq(prime_factor_dict[a]) for a in a_values])
            inner_sum = inner_sum_t1-inner_sum_t2
            s += inner_sum/euler_totient_de(prime_factor_dict[d],prime_factor_dict[e])
    return s*(1+2*C)/(1-2*C)

######################################
# Returns the (first) double sum on
# RHS of (5.2) & (5.3) mult. by log(n)
######################################
def double_sum(k_div_sets,C,n):
    s = 0
    for d,e_values in k_div_sets:
        for e in e_values:
            p = power(n,-C)
            s += (1/sqrt(n))*(1/e-1/d)+(1/sqrt(fmul(d,e)))*p+p*p
    return s*log(n)

###########################################
# Returns the RHS of (5.2) & (5.3)
# with the appropriate factor (2 & 1 resp.)
# multiplied with the main (product) term
###########################################
def calculate_overall_sum(n,k,C,c,main_term_factor,res_code):
    k_divisors,k_div_sets = create_divisor_sets(k//main_term_factor)
    main_term = main_term_factor*main_product(k_divisors,c)
    ctheta_term = ctheta_triple_sum(k_div_sets,n,res_code)
    trpl_sum = euler_totient_triple_sum(k_div_sets,C)
    dbl_sum = double_sum(k_div_sets,C,n)
    return main_term-ctheta_term-trpl_sum-dbl_sum-fdiv(log(k),n)-fdiv(log(n),n)

######################################
# Returns all the exceptions to T5.2
# for a particular even squarefree k
######################################
def find_exceptions(k):
    exceptions = []
    flag = False
    try:
        k_prime_divs = prime_factor_dict[k]
    except:
        k_prime_divs = primefactors(k)
    possible_exceptions = [2*p for p in k_prime_divs]+list(map(sum,combinations(k_prime_divs[1:],2)))
    for n in possible_exceptions:
        for p in relevant_primes[:bisect(relevant_primes,n-2)]:
            if mobius(int(n-p)) and gcd(int(n-p),k) == 1:
                flag = True
                break
        if not flag:
            exceptions.append(n)
        flag = False
    return exceptions

#############################################
# Prints the maximum exception for
# each value of even squarefree k <= 2*(10^5)
# and the maximum exception overall to T5.2
#############################################
def compile_exceptions(sqfree_no):
    max_exceptions_dict = {}
    for k in sqfree_no:
        exceptions = find_exceptions(k)
        if exceptions:
            max_exceptions_dict[k] = max(exceptions)
    print(max_exceptions_dict)
    max_k = max(max_exceptions_dict,key=max_exceptions_dict.get)
    print(max_k,max_exceptions_dict[max_k])
    return

def main():
    t1 = time()
    c = 0.373955813619202 # Artin's constant
    
    # Verify T5.2 for n >= 4*(10^18) using L5.1 (for k even and squarefree)
    n = fmul(4,power(10,18))
    C = 0.2
    sqfree_no = list(filter(lambda x: mobius(x),range(2,200000,2)))
    for k in sqfree_no:
        overall_sum = calculate_overall_sum(n,k,C,c,2,0)
        if overall_sum <= 0:
            print(k)
    # Find all exceptions for n <= 4*(10^18)
    compile_exceptions(sqfree_no)
    t2 = time()
    print(t2-t1)

    # Verify T5.5 for n >= 8*(10^9) using L5.4 (for k odd and with at most 2 prime factors)
    n = fmul(8,power(10,9))
    C = 0.37
    sqfree_no = list(filter(lambda x: mobius(x) and len(prime_factor_dict[x]) <= 2,range(1,100000,2)))
    for k in sqfree_no:
        overall_sum = calculate_overall_sum(n,k,C,c,1,1)
        if overall_sum <= 0:
            print(k)
    t3 = time()
    print(t3-t2)

    # Verify T5.6 (for k odd and squarefree)
    n = fmul(10,power(10,24))
    C = 0.2
    sqfree_no = list(filter(lambda x: mobius(x),range(1,100000,2)))
    for k in sqfree_no:
        overall_sum = calculate_overall_sum(n,k,C,c,1,2)
        if overall_sum <= 0:
            print(k)
    t4 = time()
    print(t4-t3)

if __name__ == "__main__":
    main()